# <p align="center">:computer:Discovery HTML:computer:</p>
### <p align="center">:speech_balloon: HTML - O guia estelar de HTML :speech_balloon:</p> 
#
## :balloon:Tecnologias
O curso foi desenvolvido com a linguagem:
- ``HTML``

## :memo:Linha do Tempo
- [Capitulo 1: Conceitos](https://github.com/vhbitencourtc/Discovery_HTML/blob/main/conceitos.html)
  - [Exercícios - Conceitos](https://github.com/vhbitencourtc/Discovery_HTML/blob/main/ex_conceitos.html)


## :computer:Curso
Repositório criado para armazenamento das aulas e práticas aprendidas no Discovery da RocketSeat.
> Com mentoria do `Maky brito`

## 📝Contribuições
Feito e refeito de :heart: por `mim`, [Victor H Bitencourt C](https://github.com/vhbitencourtc/):alien:
